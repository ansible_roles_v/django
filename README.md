Django
=========
Installs Django framework.  

Include role
------------
```yaml
- name: django  
  src: https://gitlab.com/ansible_roles_v/django/  
  version: main  
```

Example Playbook
----------------
```yaml
- hosts: hosts
  gather_facts: true
  become: true
  roles:
    - django
```